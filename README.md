# Monsty-Corp

A sci-fi story-based puzzle game, Monsty Corp will have you exploring the facility known as "Monsty Corp" after you wake up with no idea where you are or why you're there.

[Steam](https://store.steampowered.com/app/1335580/Monsty_Corp/)
[Twitter](https://twitter.com/monstycorpgame)
